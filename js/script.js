'use strict';

const divAuthentication = document.querySelector("#authenticate-page");
const divLogin = document.querySelector(".form-signin")
const inputLoginUser = document.querySelector("#inputLoginUser");
const inputLoginPassword = document.querySelector("#inputLoginPassword");
const divInputs = document.querySelector("#loginLabel");
const loadingElement = document.querySelector("#loadingContainer");
const linkOpenRegister = document.querySelector("#openRegisterDiv");
const submitLogin = document.querySelector("#btnLogin");
const spanLoginMsg = document.querySelector(".login-msg");
const spanLoginMsgError = document.querySelector(".login-msg-err");

const divRegister = document.querySelector(".form-register")
const inputRegisterEmail = document.querySelector("#inputRegisterEmail");
const inputRegisterNickname = document.querySelector("#inputRegisterNickname");
const inputRegisterPassword = document.querySelector("#inputRegisterPassword");
const linkOpenLogin = document.querySelector("#openLoginDiv");
const submitRegister = document.querySelector("#btnRegister");
const spanRegisterMsg = document.querySelector(".register-msg");

const divApp = document.querySelector("#app");
const btnActionCrearPost = document.querySelector("#btnActionCrearPost");
const containerPosts = document.querySelector("#posts-container");
const btnLogout = document.querySelector("#logout");

// EVENT LISTENERS
linkOpenRegister.addEventListener("click", function(e){
    e.preventDefault();
    spanLoginMsgError.textContent = "";
    divLogin.classList.toggle("d-none");
    divRegister.classList.toggle("d-none");
});
submitLogin.addEventListener("click", function(e){
    e.preventDefault();
    loginUser(inputLoginUser.value, inputLoginPassword.value);
});

linkOpenLogin.addEventListener("click", function(e){
    e.preventDefault();
    divLogin.classList.toggle("d-none");
    divRegister.classList.toggle("d-none");
});
submitRegister.addEventListener("click", function(e){
    e.preventDefault();
    registerUser(inputRegisterEmail.value, inputRegisterNickname.value, inputRegisterPassword.value);
});
btnLogout.addEventListener("click", function(e){
    e.preventDefault();
    logout();
});


/* APP */
let count_01 = 0; // contador
btnActionCrearPost.addEventListener("click", function(e){
    e.preventDefault();
    if(count_01 === 0){ // si clica por primera vez crea dom element.
        mostrarModalFormulario();
        count_01++;
    }
    else{ // si clica por segunda vez usando contador, elimina dom element.
        ocultarModalFormulario();
        count_01 = 0;
    }
});


// VARS & INITIALIZERS
const accounts = [];
let currentAccount;


