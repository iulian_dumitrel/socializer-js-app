// FUNCTIONS
const registerUser = function(email, nickname, password){
    if(nickname.length >= 3 && password.length >= 3){
        if(!accounts.some(acc => acc.nick === nickname || acc.email === email)){ // comprueba si ya existe email o nickname, si no existe, OK
            spanRegisterMsg.textContent = "";
            accounts.push({email: email, nick: nickname, pass: password, posts:[]});
            console.log(accounts);
            // Cambiamos vistas
            divLogin.classList.toggle("d-none");
            divRegister.classList.toggle("d-none");
            spanLoginMsg.textContent = `Usuario ${nickname} creado con éxito`;
        }
        else{
            spanRegisterMsg.textContent = "Usuario ya existe";
        }
    }
    else{
        spanRegisterMsg.textContent = "El nickname o contraseña son demasiado cortos";
    }
}
const loginUser = function(username, password){
    spanLoginMsg.textContent = "";
    spanLoginMsgError.textContent = "";
    currentAccount = accounts.find(acc => acc.nick === username || acc.email === username);
    console.log(currentAccount);
    if(currentAccount?.pass === password){
        divInputs.classList.add("d-none");
        loadingElement.classList.remove("d-none");
        setTimeout(mostrarPanel, 3000);
        mostrarPanelActualizado();
    }
    else{
        spanLoginMsgError.textContent = "Error de credenciales";
    }
}

const logout = function(){
    currentAccount = [];
    ocultarPanel();
}

const mostrarPanel = function(){
    divLogin.classList.add("d-none");
    divRegister.classList.add("d-none");
    divApp.classList.remove("d-none");
}

const ocultarPanel = function(){ // muestra form con registro y login
    divApp.classList.add("d-none");
    divLogin.classList.remove("d-none");
    divInputs.classList.remove("d-none");
    loadingElement.classList.add("d-none");
    divRegister.classList.add("d-none");
}

const mostrarPanelActualizado = function(){
    containerPosts.innerHTML = ''; // borra todos los posts, y los vuelve a cargar.

    let posts = accounts.map((account, index) => {
        return account.posts;
    }).flat();
    
    posts.forEach(function(post){
        html = `
        <div class="col-md-12" id="create-post">
            <div class="card-publicacion mt-2  my-1 d-flex flex-row justify-content-between p-3">
                <p class="mb-1">${post}</p>
            </div>
        </div>`;
        containerPosts.insertAdjacentHTML('afterbegin', html);
        console.log(currentAccount.posts);
    });
}

/* Creación post */
const mostrarModalFormulario = function(){
    let html = `
    <div class="col-md-12" id="create-post">
        <div class="card-publicacion mt-3 px-2">
            <p class="text-danger mb-0 mt-1" id="createPostMessage"></p>
            <textarea class="form-control mb-2" maxlength="180" id="post_message" rows="3" placeholder="Introduce tu mensaje aquí"></textarea>
            <button class="btn btn-primary radio-20 w-25 mb-2" id="btnCrearPost">Crear</button>
        </div>
    </div>`;
    containerPosts.insertAdjacentHTML('afterbegin', html);
    document.querySelector("#btnCrearPost").addEventListener("click", function(e){
        e.preventDefault();
        let postValue = document.querySelector("#post_message").value;
        crearPost(postValue);
    });
}

const ocultarModalFormulario = function(){
    document.querySelector("#create-post").remove();
}

const crearPost = function(postValue){
    if(postValue.length <= 180 && postValue.length >= 5){ // validacion para que los textos no superen limites
        currentAccount.posts.push(postValue);
        ocultarModalFormulario();
        count_01 = 0;
        mostrarPanelActualizado();
    }else{
        document.getElementById("createPostMessage").textContent = "Los campos no pueden estar vacios";
    }
}
